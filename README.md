# AC_Trigger PCB

Design files for AC Trigger board to convert low-frequency (<100kHz) signals to TTL pulse.
Used to primarily synchronize experiments to powerline phase.

# Libraries
This project uses the [DQC KiCAD Libraries](https://gitlab.com/duke-artiq/pcb/dqc-kicad-libraries)